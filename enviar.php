<?php

  $para      = 'info@universidaddelascompetencias.pe'; //Aca el correo a donde quieres que llegue
  $titulo    = 'Nuevo envio desde el formulario de Contacto';

  $mensaje   = 'Datos de Contacto:'. "\r\n" ;
  foreach($_REQUEST as $l => $v){
    if(in_array($l,array("Nombre","Email","Telefono","Curso"))){
      $mensaje   .= $l.':  '. $v."\r\n" ;
    }
  }

  //Reemplaza el correo webmaster@example.com por uno que tenga el mismo dominio desde donde se manda

  $cabeceras = 'From: info@universidaddelascompetencias.pe' . "\r\n" .
      'Reply-To: info@universidaddelascompetencias.pe' . "\r\n";

  //Esto envia el formulario
  mail($para, $titulo, $mensaje, $cabeceras);

?>
