<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Master UCP</title>

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="shortcut icon" type="image/x-icon" href="http://universidaddelascompetencias.pe/home/sites/default/files/favicon_ultimo.ico" />
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="css/custom.css">
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
	<header>
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<img src="img/logo-ucp.png" alt="">
				</div>
				<div class="col-md-3">
					<img src="img/logo-unesco.png" alt="">
				</div>
				<div class="col-md-4">
					<img src="img/logo-inventory.png" alt="">
				</div>
			</div>
		</div>
	</header>

	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<h2 class="title-ucp">Maestrías y <br><span>Posgrados</span></h2>
				</div>
				<div class="col-md-3">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation">
							<a href="#home" aria-controls="home" role="tab" data-toggle="tab">MAESTRÍAS</a>
						</li>
						<li role="presentation" class="active">
							<a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">CONTÁCTANOS</a>
						</li>
					</ul>
				</div>

				<div class="col-md-12">
					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane fade in" id="home">

							<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
								<h2 class="title-tab">Área de Ciencias Empresariales</h2>
								<!-- Collapse 1 -->
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingOne">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
												Máster en Recursos Humanos y Relaciones Laborales
											</a>
										</h4>
									</div>
									<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
										<div class="panel-body">
											<p>
												Este Master te permitirá incrementar tu capacidad de análisis, descripción y valoración de puestos de trabajo, evaluación del desempeño y gestión por competencias, gestión estratégica de recursos humanos, estructura organizativa y dirección estratégica, habilidades personales y directivas.
											</p>
											<p class="text-right">

												<a class="green-txt" href="pdf/area-de-ciencias-empresariales-salud/master-relaciones-laborales.pdf" target="_blank"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Descargar Programa</a>

												<a class="green-txt" href="pdf/TEL-UCP-master-relaciones-laborales.pdf" target="_blank"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Descargar Programa</a>

											</p>
										</div>
									</div>
								</div>

								<!-- Collapse 2 -->
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingTwo">
										<h4 class="panel-title">
											<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
												Máster en Negocios Internacionales
											</a>
										</h4>
									</div>
									<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
										<div class="panel-body">
											<p>
												El Master te permitirá gestionar el factor humano en empresas internacionales, gestionar el entorno económico, realizar marketing internacional, realizar una gestión intercultural y gestionar proyectos internacionales
											</p>
											<p class="text-right">

												<a class="green-txt" href="pdf/area-de-ciencias-empresariales-salud/master-negocios-internacionales.pdf" target="_blank"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Descargar Programa</a>

												<a class="green-txt" href="pdf/TEL-UCP-master-negocios-internacionales.pdf" target="_blank"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Descargar Programa</a>

											</p>
										</div>
									</div>
								</div>

								<!-- Collapse 3 -->
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingThree">
										<h4 class="panel-title">
											<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
												Máster en Dirección y Administración de Empresas (MBA)
											</a>
										</h4>
									</div>
									<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
										<div class="panel-body">
											<p>
												Formación para que como directivo, seas capaz de evaluar la idoneidad de una estrategia comercial, analizando el plan de marketing en el que se basa, decidiendo los canales de comercialización más adecuados, etc. garantizando así los resultados deseados para su empresa.
											</p>
											<p class="text-right">

												<a class="green-txt" href="pdf/area-de-ciencias-empresariales-salud/master-en-direccion-y-administracion-de-empresas.pdf" target="_blank"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Descargar Programa</a>

												<a class="green-txt" href="pdf/TEL-UCP-master-en-direccion-y-administracion-de-empresas.pdf" target="_blank"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Descargar Programa</a>

											</p>
										</div>
									</div>
								</div>

								<h2 class="title-tab">Área de Ciencias de la Educación</h2>

								<!-- Collapse 4 -->
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingFour">
										<h4 class="panel-title">
											<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
												Especialización en Neuropsicología Inteligencias Múltiples y Mindfulnes- Jóvenes
											</a>
										</h4>
									</div>
									<div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
										<div class="panel-body">
											<p>
												Conocer las aportaciones del Mindfulness, las Inteligencias Múltiples y la Neurociencia, su sustento empírico y las diferentes áreas profesionales que las aplican.
											</p>
											<p class="text-right">
												<a class="green-txt" href="pdf/area-de-ciencias-de-la-educacion/maestria-neuropsicologia-mindfulness-jovenes-y-niños.pdf" target="_blank"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Descargar Programa</a>
											</p>
										</div>
									</div>
								</div>


								<h2 class="title-tab">Área Sostenibilidad con el Medioambiente</h2>
								<!-- Collapse 5 -->
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingFive">
										<h4 class="panel-title">
											<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
												Máster en Liderazgo Sostenible
											</a>
										</h4>
									</div>
									<div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
										<div class="panel-body">
											<p>
												Aprenderás Describir el Desarrollo Sostenible para definir un plan de sostenibilidad desde las cuatro esferas del DS: medio ambiente, sociedad, economía y cultura, así como desde los temas sobre sostenibilidad que sean importantes para la comunidad o incluso el país
											</p>
											<p class="text-right">

												<a class="green-txt" href="pdf/area-sostenibilidad-con-el-medioambiente/master-en-liderazgo-sostenible.pdf" target="_blank"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Descargar Programa</a>

												<a class="green-txt" href="pdf/TEL-UCP-master-en-liderazgo-sostenible-Unesco.pdf" target="_blank"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Descargar Programa</a>

											</p>
										</div>
									</div>
								</div>
								
								<!-- Collapse 6 -->
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingSix">
										<h4 class="panel-title">
											<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
												Máster en Desarrollo Sostenible para Entidades Locales
											</a>
										</h4>
									</div>
									<div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
										<div class="panel-body">
											<p>
												CLograr el autoconocimiento del alumno, tanto a nivel de habilidades psicológicas como de competencias emocionales para desarrollar su autoconfianza, y ser capaz de permanecer en su propio equilibrio personal ante cualquier circunstancia.
											</p>
											<p class="text-right">

												<a class="green-txt" href="pdf/area-sostenibilidad-con-el-medioambiente/master-en-desarrollo-sostenible-para-entidades-locales.pdf" target="_blank"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Descargar Programa</a>

												<a class="green-txt" href="pdf/TEL-UCP-master-en-desarrollo-sostenible-para-entidades-locales-Unesco.pdf" target="_blank"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Descargar Programa</a>

											</p>
										</div>
									</div>
								</div>

								<!-- Collapse 7 -->
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingSeven">
										<h4 class="panel-title">
											<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
												Máster en Desarrollo Sostenible en la Empresa
											</a>
										</h4>
									</div>
									<div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
										<div class="panel-body">
											<p>
												Aprenderás a describir métodos para integrar el Desarrollo Sostenible en la estrategia de la empresa, explotando el business plan desde las tres esferas de la sostenibilidad: medio ambiente, sociedad y economía.
											</p>
											<p class="text-right">

												<a class="green-txt" href="pdf/area-sostenibilidad-con-el-medioambiente/master-en-desarrollo-sostenible-de-la-empresa.pdf" target="_blank"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Descargar Programa</a>

												<a class="green-txt" href="pdf/TEL-UCP-master-en-desarrollo-sostenible-de-la-empresa-Unesco.pdf" target="_blank"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Descargar Programa</a>

											</p>
										</div>
									</div>
								</div>

								<!-- Collapse 8 -->
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingEight">
										<h4 class="panel-title">
											<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
												Máster en Calidad, Higiene y Seguridad Alimentaria Online
											</a>
										</h4>
									</div>
									<div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
										<div class="panel-body">
											<p>
												Conocerás todo lo relativo a la Alimentación, Nutrición y Dietética, así como los fundamentos de la microbiología de los alimentos y los factores que influyen en su desarrollo y actividad. Estudiaras el marco normativo del sector alimentario, la legislación y sus normas.
											</p>
											<p class="text-right">
												<a class="green-txt" href="pdf/area-sostenibilidad-con-el-medioambiente/maestria-en-gestion-de-la-calidad-y-la-seguridad-en-la-industria.pdf" target="_blank"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Descargar Programa</a>
											</p>
										</div>
									</div>
								</div>

							</div>
						</div>

						<div role="tabpanel" class="tab-pane fade in  active" id="profile">
							<div class="row">
								<div class="col-md-5">
									<div class="row">
										<div class="col-md-6 formulario-ucp">
											<form action="" class="form-ucp">
												<h4>Tú pones el horario, nosotros la experiencia </h4>
												<h3>¡Dale un nuevo impulso a tu carrera!</h3>
												<div class="form-group">
													<input type="text" required class="form-control" id="fieldName" placeholder="Nombre y Apellido" name="Nombre" required>
												</div>

												<div class="form-group">
													<input type="email" required class="form-control" id="fieldMail" placeholder="E-mail" name="Email" required>
												</div>

												<div class="form-group">
													<input type="text" required class="form-control" id="fieldPhone" placeholder="Teléfono" name="Telefono" required>
												</div>

												<div class="form-group">
													<select class="form-control" name="Curso" id="" required>
														<option value="">--Seleccionar curso--</option>
														<option value="1">Máster en Recursos Humanos y Relaciones Laborales</option>
														<option value="2">CMáster en Negocios Internacionales</option>
														<option value="3">Máster en Dirección y Administración de Empresas (MBA)</option>
														<option value="4">Especialización en Neuropsicología Inteligencias Múltiples y Mindfulnes- Jóvenes</option>
														<option value="5">Máster en Liderazgo Sostenible</option>
														<option value="5">Máster en Desarrollo Sostenible para Entidades Locales</option>
														<option value="5">Máster en Desarrollo Sostenible en la Empresa</option>
														<option value="5">Máster en Calidad, Higiene y Seguridad Alimentaria Online</option>
													</select>
												</div>
												<div class="checkbox">
													<label>
														<input type="checkbox"> 
														<a href="#" data-toggle="modal" class="link green-txt" data-target="#terminos">Terminos y condiciones</a>
													</label>
												</div>
												<hr>
												<button class="btn btn-green" href="#" role="button">Solicitar información</button>
											</form>
										</div>
									</div>

								</div>
								<div class="col-md-4 col-md-offset-3"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<!-- Modal -->
	<div class="modal fade" id="terminos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Términos y Condiciones</h4>
	      </div>
	      <div class="modal-body">
			<h2>POLITICA DE PRIVACIDAD Y DE PROTECCION DE DATOS PERSONALES</h2>
			<p><strong>“TELEFÓNICA LEARNING SERVICES PERÚ S.A.C.”</strong>,  en adelante, <strong>TLS</strong>, es el titular del banco de datos personales en el cual se recopilará todos los datos personales brindados en el formulario del Registro “Contacto”, así como de los datos personales que facilite de manera directa o indirecta.</p>
			 
			<p>Mediante la aceptación de esta política de privacidad y de protección de datos personales, Ud. acepta y consiente de manera expresa a TLS tratar los datos personales que proporcione para los siguientes fines: responder consultas acerca de productos y servicios, atención de reclamos, para el cumplimiento de obligaciones ante una eventual obligación contractual entre TLS y el usuario, para el envío de publicidad mediante cualquier medio y soporte, envío de invitaciones a actividades convocadas por TLS o sus socios comerciales y para fines estadísticos. Todos los campos de registro son de llenado obligatorio, sin el llenado de los campos obligatorios no se podrá realizar el envío de la consulta.</p>
			 
			<p>El TLS asegura tratar sus datos personales cumpliendo diligentemente las obligaciones establecidas en la Ley 29733, Ley de protección de datos personales y demás normas complementarias, en especial las referidas a la guarda de confidencialidad de la información y aplicación de las medidas de seguridad pertinentes.</p>
			 
			<p>El titular del dato personal o su representante podrá presentar la solicitud de ejercicio de sus derechos reconocidos en la Ley 29733 utilizando alguno de los siguientes canales de comunicación:</p>

			<p>Escribiendo a cau@tedsoporte o a la siguiente dirección: Av. Camino Real 155 Piso 4 – San Isidro</p>
			<p>Llamando al teléfono 2224760 en horario de Lunes a Viernes de 09:00 am a 05:30 pm.</p>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
	      </div>
	    </div>
	  </div>
	</div>
	<footer id="socios-ucp">
		<div class="container">
			<div class="row">
				<div class="col-md-2">
					<h4>Socios estratégicos</h4>
				</div>
				<div class="col-md-2"><img src="img/logo-imf.png" alt=""></div>
				<div class="col-md-2"><img src="img/logo-senati.png" alt=""></div>
				<div class="col-md-2"><img src="img/logo-cvn.png" alt=""></div>
				<div class="col-md-2"><img src="img/logo-upb.png" alt=""></div>
				<div class="col-md-2"><img src="img/logo-cayetano.png" alt=""></div>
			</div>
		</div>
	</footer>
	<footer id="powered-by-ted">
		<div class="container">
			<div class="row">
				<div class="col-md-10 text-right">
					<p>Powered by:</p>
				</div>
				<div class="col-md-2 text-left">
					<img src="img/logo-ted.png" alt="">
				</div>
			</div>
		</div>
	</footer>
	<div class="modal fade" id="message-send" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-body">
	      	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      	<p id="name-user">Estimado Usuario</p>

	      	<h3>¡Muchas gracias por registrarte!</h3>

			<p>En breve nos comunicaremos contigo.</p>

			<a href="http://universidaddelascompetencias.pe/home/" class="btn-green padding-10">Visítanos en nuestra Web</a>
	      </div>
	    </div>
	  </div>
	</div>

	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script>
		(function($){
			$(document).ready(function(){
				$('#someTab').tab('show');
				$('.form-ucp').on('submit',function(e){
					e.preventDefault();
					var $this = $(this),
						data = $this.serialize();
						$this.find('input[type="text"], input[type=email]').val('');
						$this.find('select').val('');
						$this.find('input[type="checkbox"]').prop('checked',false);

						$.ajax({
							url:'enviar.php',
							method:'post',
							data:data,
							success:function(){
								$('#message-send').modal()
							}
						})

				});
			});
		})(jQuery);
	</script>
</body>
</html>
